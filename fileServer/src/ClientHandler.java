import java.io.*;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class ClientHandler implements Runnable {
    private Socket socket;
    private String address;
    private PrintWriter out;
    private BufferedReader in;

    // static file locations
    public static final String FILE_LOCATION = "./src/serverFiles/";
    public static final String KEY_LOCATION = "./src/keys/";
    public static final String PRIVATE_KEY = "privateKey.key";
    public static final String PUBLIC_KEY = "publicKey.pub";

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Starting ClientHandler.");
        // Read config and extract file paths

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            address = socket.getInetAddress().getHostAddress();
            System.out.printf("Client connected from %s:%d%n", address, socket.getPort());

            // Generate key pair.
            FileServerFunctions.keyGen(new File(KEY_LOCATION), "privateKey", "publicKey");
            // Send public key to client.
            FileServerFunctions.sendKey(socket, new File(KEY_LOCATION));

            commandPrompt(out);
            requestInput(out);

            String inputLine;

            while ((inputLine = in.readLine()) != null) { // loop to handle continuous input from client
                if (inputLine.equalsIgnoreCase("Reset"))// signals end of file transfer
                {
                    commandPrompt(out);
                    requestInput(out);

                } else {// handle commands from user

                    String[] command = inputLine.split(" ");// split string into command with args

                    // receive and decrypt file
                    if (command[0].equalsIgnoreCase("send")) try {
                        out.println("send"); // Invoke client file send .
                        out.println(command[1]);

                        int fileSize = Integer.parseInt(in.readLine());
                        FileServerFunctions.receiveFile(socket, out, new File(FILE_LOCATION), command[1], fileSize);

                        File encryptedFile = new File(FILE_LOCATION + command[1]);

                        if (encryptedFile.exists()) {
                            try {
                                FileServerFunctions.decryptFile(new File(FILE_LOCATION), command[1], new File(KEY_LOCATION), PRIVATE_KEY);
                            } catch (InvalidKeySpecException e) {
                                e.printStackTrace();
                            }
                            if (encryptedFile.delete()) { // delete cypher text after decryption
                                System.out.println("Cipher text cleared");
                            } else {
                                System.out.println("Unable to clear Cipher text");
                            }
                        }

                        commandPrompt(out);
                        requestInput(out);

                    } catch (IOException e) {
                        System.err.println("An I/O error has occurred.");
                    }
                    else if (command[0].equalsIgnoreCase("bye")) {// client manually disconnects
                        out.println("bye");
                        break;
                    }
                }
            }

            // Close all resources.
            out.close();
            in.close();
            socket.close();
            FileServerFunctions.deleteKeys(new File(KEY_LOCATION),PRIVATE_KEY, PUBLIC_KEY);

            System.out.println("The client disconnected.");

        } catch (IOException e) {
            System.err.println("Connection to the client has been lost.");
            FileServerFunctions.deleteKeys(new File(KEY_LOCATION),PRIVATE_KEY, PUBLIC_KEY);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends request for input flag to client.
     *
     * @param writer
     */
    private void requestInput(PrintWriter writer) {
        writer.println("ready");
    }

    /**
     * Sends text prompt for user input to client.
     *
     * @param writer
     */
    private void commandPrompt(PrintWriter writer) {
        writer.println("Enter 'send' + 'filename' ('bye' to quit): ");
    }

}
