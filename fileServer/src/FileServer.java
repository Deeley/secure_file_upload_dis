import java.net.Socket;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileServer
{
    private ServerSocket serverSocket;
    private ExecutorService executorService;
    private int port = 8888;
    private int poolSize = 30;

    public void initialize()
    {
        System.out.printf("FileServer started on port: %d%n", port);

        try {
            serverSocket = new ServerSocket(port); // Initialise the serverSocket socket.

        } catch (IOException e) {

            System.out.printf("The port %d is not available %n", port);
            System.out.println("Please ensure the selected port is free.");
            System.exit(-1);
        }

        executorService = Executors.newFixedThreadPool(poolSize); // Thread pool facilitates concurrent clients.

        while (true) { // Wait for clients to connect
            try {
                Socket secureClient = serverSocket.accept(); // Accept client connection.
                executorService.submit(new ClientHandler(secureClient)); // Create client handler for each connection.

            } catch (IOException e) {

                System.err.println("Unable to instantiate client handler");
            }
        }
    }

    public static void main(String[] args)
    {
        FileServer instance = new FileServer();
        instance.initialize();
    }
}
