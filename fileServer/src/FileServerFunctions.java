import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

@SuppressWarnings("Duplicates")

public class FileServerFunctions {


    /**
     * Receives decrypted cipher blocks and assembles plain text file.
     * Available at: https://github.com/jaysridhar/java-stuff/tree/master/source/rsa-encryption
     *
     * @param cipher Receives the blocks of cipher text from AES decryption.
     * @param in Initialisation vector dictates the beginning of AES cipher.
     * @param out Output to file writer.
     *
     * @throws javax.crypto.IllegalBlockSizeException
     * @throws javax.crypto.BadPaddingException
     * @throws java.io.IOException
     */
    static private void buildFile(Cipher cipher,InputStream in,OutputStream out)
            throws javax.crypto.IllegalBlockSizeException,
            javax.crypto.BadPaddingException,
            java.io.IOException
    {
        byte[] inputBuffer = new byte[1024];
        int length;
        while ((length = in.read(inputBuffer)) != -1) {
            byte[] outputBuffer = cipher.update(inputBuffer, 0, length);
            if ( outputBuffer != null ) out.write(outputBuffer);
        }
        byte[] outputBuffer = cipher.doFinal();
        if ( outputBuffer != null ) out.write(outputBuffer);
    }

    /**
     * Generates asymmetric key pair for use in encryption.
     *
     * @param location Location for keys to be written to.
     * @param privkeyName Name of private key file to be created.
     * @param pubkeyName Name of public key file to be created.
     *
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.io.IOException
     */
    public static void keyGen(File location, String privkeyName, String pubkeyName)
            throws java.security.NoSuchAlgorithmException,
            java.io.IOException {

        // Create 2048 bit RSA key pair
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        File privateKey = new File(location.getPath() + "/" + privkeyName + ".key");
        File publicKey = new File(location.getPath() + "/" + pubkeyName + ".pub");

        // Write private key to File
        try (FileOutputStream out = new FileOutputStream(privateKey)) {
            out.write(keyPair.getPrivate().getEncoded());
        }

        // Write public key to File
        try (FileOutputStream out = new FileOutputStream(publicKey)) {
            out.write(keyPair.getPublic().getEncoded());
        }
        System.out.println("Key pair generated.");
    }

    /**
     * Sends public key across the network via a socket.
     *
     * @param socket The socket used to send bytes.
     * @param location Location of key being transmitted.
     */
    public static void sendKey(Socket socket, File location) {
        try {
            File publicKey = new File(location.getPath() + "/" + "publicKey.pub"); // The file to send.

            if (publicKey.exists()) {

                Thread.sleep(1000);

                try (FileInputStream fileInputStream = new FileInputStream(publicKey)) {

                    byte[] buffer = new byte[8 * 1024]; // Buffer for bytes of the file.
                    int totalSize = 0;
                    int count;

                    // Write bytes to outbound socket.
                    while ((count = fileInputStream.read(buffer)) > 0) {

                        totalSize += count;
                        socket.getOutputStream().write(buffer, 0, count); // Write bytes.
                    }
                } catch (FileNotFoundException e) {
                    System.err.printf("Unable to write key to file.");
                }
            } else {
                System.err.printf("Key could not be located.");
            }
            System.out.println("Public key sent to client.");
        } catch (IOException e) {
            System.err.println("I/O error.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receives and builds an incoming file via network through a specified socket.
     *
     * @param socket The socket from which the bytes will be received.
     * @param out Outbound socket to send finished flag to server.
     * @param location Location of the file being written to.
     * @param fileName Name of the file being written to.
     * @param fileSize Size of file being written.
     */
    public static void receiveFile(Socket socket, PrintWriter out, File location, String fileName, int fileSize)
    {
        File incomingFile = new File(location.getPath() + "/" + fileName);

        try (FileOutputStream fileOutputStream = new FileOutputStream(incomingFile))
        {
            if (!incomingFile.exists()) // creates empty file to write data to if one is not present
            {
                incomingFile.createNewFile();
            }

            byte[] buffer = new byte[8 * 1024]; // Buffer stores bytes of incoming file.
            int totalSize = 0;
            int bytesRead;

            // Read all bytes in file.
            while (totalSize < fileSize && (bytesRead = socket.getInputStream().read(buffer)) != -1)
            {
                totalSize += bytesRead;
                fileOutputStream.write(buffer, 0, bytesRead); // Write bytes to fileOutput.
            }
            out.println("Reset");
        } catch (IOException e)
        {
            System.err.println("I/O error.");
        }
    }

    /**
     * Decrypts cipher text using appropriate private key.
     *
     * @param fileLocation Location of cipher text file.
     * @param fileName Name of cipher text file
     * @param keyLocation Location of private key.
     * @param keyName File name of private key.
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws InvalidKeySpecException
     */
    public static void decryptFile(File fileLocation, String fileName, File keyLocation, String keyName)
            throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {


        File pvtKeyFile = new File(keyLocation.getPath() + "/" + keyName);

        // initialize byte array to contain private key
        byte[] bytes = Files.readAllBytes(pvtKeyFile.toPath()); // characters stored in external file
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

        File fileToDecrypt = new File(fileLocation.getPath() + "/" +fileName);

        // trim '.enc' extension to write decrypted cipher text to readable file format
        String outputFileName = fileToDecrypt.getName();
        if(outputFileName.indexOf(".") > 0)
            outputFileName = outputFileName.substring(0, outputFileName.lastIndexOf("."));

        File outputFile = new File(fileLocation.getPath() + "/" + outputFileName);

        try (FileInputStream in = new FileInputStream(fileToDecrypt)) {
            SecretKeySpec secretKey = null;
            {
                // RSA decryption of AES key
                Cipher RSAcipher = Cipher.getInstance("RSA/ECB/PKCS1Padding"); // Create cipher instance.
                RSAcipher.init(Cipher.DECRYPT_MODE, privateKey);
                byte[] b = new byte[256];
                in.read(b); // Initialisation vector marks end of AES key in payload
                byte[] keyBytes = RSAcipher.doFinal(b);
                secretKey = new SecretKeySpec(keyBytes, "AES");
            }

            byte[] iv = new byte[128/8];
            in.read(iv);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            Cipher AEScipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            AEScipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec); // Retrieves cipher of file using AES key

            try (FileOutputStream out = new FileOutputStream(outputFile)){ // Writes decrypted cipher blocks to file.
                buildFile(AEScipher, in, out);
            }
        } catch (NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
    }
    // delete key pair.
    public static void deleteKeys(File keyLocation, String privateKey, String publicKey){
        File pubKeyFile = new File(keyLocation.getPath() + "/" + publicKey);
        pubKeyFile.delete();

        File pvtKeyFile = new File(keyLocation.getPath() + "/" + privateKey);
        pvtKeyFile.delete();
    }
}
