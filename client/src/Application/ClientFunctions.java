package Application;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;

@SuppressWarnings("Duplicates")

public class ClientFunctions {

    static SecureRandom secureRandom = new SecureRandom(); // secure random seed

    /**
     * Receives blocks of cipher text and writes to file output.
     * Available at: https://github.com/jaysridhar/java-stuff/tree/master/source/rsa-encryption
     *
     * @param cipher Receives the blocks of cipher text from AES decryption.
     * @param in Initialisation vector dictates the beginning of AES cipher.
     * @param out Output to file writer.
     *
     * @throws javax.crypto.IllegalBlockSizeException
     * @throws javax.crypto.BadPaddingException
     * @throws java.io.IOException
     */
    static private void buildFile(Cipher cipher, InputStream in, OutputStream out)
            throws javax.crypto.IllegalBlockSizeException,
            javax.crypto.BadPaddingException,
            java.io.IOException
    {
        byte[] inputBuffer = new byte[1024];
        int length;
        while ((length = in.read(inputBuffer)) != -1) {
            byte[] outputBuffer = cipher.update(inputBuffer, 0, length);
            if ( outputBuffer != null ) out.write(outputBuffer);
        }
        byte[] outputBuffer = cipher.doFinal();
        if ( outputBuffer != null ) out.write(outputBuffer);
    }

    /**
     * Transfers bytes of file to server across network.
     *
     * @param socket Socket used to send bytes.
     * @param socketOutput Output to broadcast file information to server.
     * @param location Location of file being transferred.
     * @param fileName Name of file being transferred.
     */
    public static void fileTransfer(Socket socket, PrintWriter socketOutput, File location, String fileName) {
        try {
            File fileToSend = new File(location.getPath() + "/" + fileName); // The file to send.

            if (fileToSend.exists()) {

                socketOutput.println(fileToSend.length());

                Thread.sleep(1000);

                try (FileInputStream FIStream = new FileInputStream(fileToSend)) {

                    byte[] buffer = new byte[8 * 1024]; // Buffer for bytes of the file.
                    int total = 0;
                    int counter;

                    // Write bytes of file to outward socket.
                    while ((counter = FIStream.read(buffer)) > 0) {
                        try {
                            total += counter;
                            socket.getOutputStream().write(buffer, 0, counter); // Write bytes.
                        } catch (IOException e) {
                            System.err.println("Unable to write to socket.");
                        }
                    }
                } catch (FileNotFoundException e) {
                    System.err.printf("'%s' could not be located.%n", fileToSend.getName());
                }
            }
        } catch (IOException e) {
            System.err.println("File failed to send");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receives and builds bytes of key being transmitted during handshake with server.
     *
     * @param socket Socket being used to receive bytes.
     * @param location Location for key to be written to.
     */
    public static void receiveKey(Socket socket, File location)
    {
        File key = new File(location.getPath() + "/" + "publicKey.pub");

        try (FileOutputStream fileOutputStream = new FileOutputStream(key))
        {
            if (!key.exists()) // creates empty file to write data to if one is not present
            {
                key.createNewFile();
            }

            byte[] buffer = new byte[8 * 1024]; // Buffer stores bytes of incoming file.
            int total = 0;
            int counter;

            // Read all bytes in file.
            while (total < 294 && (counter = socket.getInputStream().read(buffer)) != -1)
            {
                total += counter;
                fileOutputStream.write(buffer, 0, counter); // Write bytes to fileOutput.
            }
            System.out.println("Key received.");

        } catch (IOException e)
        {
            System.err.println("Key exchange failed.");
        }
    }

    /**
     * Turns specified file to cipher text using public key.
     *
     * @param fileLocation Location of file to be encrypted.
     * @param fileName Name of File being encrypted.
     * @param keyLocation Location of public key.
     * @param keyName File name of public key.
     *
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.InvalidAlgorithmParameterException
     * @throws java.security.InvalidKeyException
     * @throws java.security.spec.InvalidKeySpecException
     * @throws javax.crypto.NoSuchPaddingException
     * @throws javax.crypto.BadPaddingException
     * @throws javax.crypto.IllegalBlockSizeException
     * @throws java.io.IOException
     */
    public static void encryptFile(File fileLocation, String fileName, File keyLocation, String keyName)
            throws java.security.NoSuchAlgorithmException,
            java.security.spec.InvalidKeySpecException,
            javax.crypto.NoSuchPaddingException,
            javax.crypto.BadPaddingException,
            java.security.InvalidAlgorithmParameterException,
            java.security.InvalidKeyException,
            javax.crypto.IllegalBlockSizeException,
            java.io.IOException
    {
        // Generate 128 bit AES key
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(128);
        SecretKey secretKey = keyGen.generateKey();

        File fileToEncrypt = new File(fileLocation.getPath() + "/" + fileName);
        File pubKeyFile = new File(keyLocation.getPath() + "/" + keyName);

        // Byte array containing public key
        byte[] bytes = Files.readAllBytes(pubKeyFile.toPath());
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);

        byte[] iv = new byte[128/8];
        secureRandom.nextBytes(iv); // Secure random seed IV for increased cipher strength
        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);

        try (FileOutputStream out = new FileOutputStream(fileToEncrypt + ".enc")) {
            {
                // RSA encryption of 128 bit AES key forms first section of payload
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                byte[] b = cipher.doFinal(secretKey.getEncoded());
                out.write(b);// Writes key to beginning of payload
            }

            out.write(iv);// Writes IV to file for decryption

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParamSpec);
            try (FileInputStream in = new FileInputStream(fileToEncrypt)) {// Writes cipher text to file.
                buildFile(cipher, in, out);
            } catch(IOException e){
                System.err.println("Unable to write to output file.");
            }
        }
    }
}
