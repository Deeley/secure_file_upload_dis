package Application;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class SecureClient {

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    // Static file locations.
    public static final String FILE_LOCATION = "./src/Application/clientFiles/";
    public static final String KEY_LOCATION = "./src/Application/keys/";
    public static final String PUBLIC_KEY = "publicKey.pub";

    public void start() {
        try {
            // Initialize resources.
            socket = new Socket("localhost", 8888);// Specifies location and port of file server.
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            System.out.printf("Contacting file server at: %s, port: %d...%n", socket.getInetAddress().getHostName(), socket.getPort());

        } catch (UnknownHostException e) {
            System.err.println("IP address is not reachable.");
            System.exit(-1);
        } catch (IOException e) {
            System.err.println("Couldn't establish connection to the host.");
            System.exit(-1);
        }

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));// Stores userInput from command line.

        // Receive public key
        ClientFunctions.receiveKey(socket, new File(KEY_LOCATION));

        try {// Communication protocol with file server

            String serverResponse;

            while ((serverResponse = in.readLine()) != null) {
                if (serverResponse.equalsIgnoreCase("bye")){ // Exit program.

                    System.out.println("Connection terminated");
                    break; // Exits try and closes resources

                } else if (serverResponse.equalsIgnoreCase("ready")){ // Request for input from server.

                    String userResponse = input.readLine();

                    if (userResponse != null) {

                        String[] command = userResponse.split(" "); // Splits userInput to retrieve command params.

                        if (command[0].equalsIgnoreCase("send")){

                            File file = new File(FILE_LOCATION + command[1]);

                            if (!file.exists()){
                                System.err.printf("'%s' could not be located.%n", command[1]);
                                out.println("Reset");

                            } else {
                                // Encrypt file.
                                ClientFunctions.encryptFile(new File(FILE_LOCATION), command[1], new File(KEY_LOCATION), PUBLIC_KEY);
                                // Send payload name to server.
                                out.println("send" + " " + command[1] + ".enc");
                            }
                        } else if (command[0].equalsIgnoreCase("bye")){
                            // Send bye flag to server, server will reply with same flag to terminate connection on both sides.
                            out.println("bye");

                        } else { // user inputs an invalid command
                            System.err.println("Invalid command. Please try again.");
                            out.println("Reset");
                        }
                    }

                } else if (serverResponse.equalsIgnoreCase("send")) { // Send encrypted file to the server.
                    String fileName = in.readLine();
                    ClientFunctions.fileTransfer(socket, out, new File(FILE_LOCATION), fileName);

                    File file = new File(FILE_LOCATION + fileName);
                    file.delete(); // delete encrypted once sent
                    System.err.println("Success... file transmitted ");
                } else if (!serverResponse.equalsIgnoreCase("Reset")){ // Print reply from server, except if it is "finished".

                    System.out.println(serverResponse);
                }
            }

            // Close sockets and communication resources.

            input.close();
            socket.close();
            out.close();
            in.close();

            // delete public key once client terminates.
            File key = new File(KEY_LOCATION + PUBLIC_KEY);
            key.delete();

        } catch (IOException e) {
            System.err.println("An error occurred during communication with the server. ");
            System.err.println("Please restart the client.");
            System.exit(-1);
        } catch (NoSuchAlgorithmException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | InvalidKeySpecException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // Creates client instance on startup
        SecureClient instance = new SecureClient();
        instance.start();
    }
}
